﻿using System;
namespace OOP
{
	class ResultV1
	{
		public int percent { get; set; }
		public void PrintHonors()
		{
			if (percent < 50)
			{
				Console.WriteLine("niet geslaagd");
			}
			else if (percent < 68)
			{
				Console.WriteLine("voldoende");
			}
			else if (percent < 75)
			{
				Console.WriteLine("onderscheiding");
			}
			else if (percent < 85)
			{
				Console.WriteLine("grote onderscheiding");
			}
			else
			{
				Console.WriteLine("grootste onderscheiding");
			}
		}
		public static void ResultMain()
		{
			ResultV1 result = new ResultV1();
			result.percent = 20;

			ResultV1 result1 = new ResultV1();
			result1.percent = 51;

			ResultV1 result2 = new ResultV1();
			result2.percent = 69;

			ResultV1 result3 = new ResultV1();
			result2.percent = 76;

			ResultV1 result4 = new ResultV1();
			result4.percent = 95;
			result.PrintHonors();
			result1.PrintHonors();
			result2.PrintHonors();
			result3.PrintHonors();
			result4.PrintHonors();
		}
	}
	
}

