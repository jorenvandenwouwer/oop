﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace OOP
{
    class LerenWerkenMetDateTime
    {
        public static void DayOfWeek()
        {
            Console.Write("Welke dag?");
            int day = int.Parse(Console.ReadLine());
            Console.Write("Welke maand?");
            int month = int.Parse(Console.ReadLine());
            Console.Write("Welk jaar?");
            int year = int.Parse(Console.ReadLine());
            DateTime dateTime = new DateTime(year, month, day);
            CultureInfo belgianCI = new CultureInfo("nl-BE");
            Console.WriteLine($"{dateTime.ToString("dd MMMM yyyy", belgianCI)} is een {dateTime.ToString("dddd", belgianCI)}");
        }

        public static void LeapYear()
        {
            int numberOfLeapYears = 0;
            for (int i = 1800; i <= 2020; i++)
            {
                if (DateTime.IsLeapYear(i))
                {
                    numberOfLeapYears++;
                }
            }
            Console.WriteLine($"Er zijn {numberOfLeapYears} schrikkeljaren tussen 1800 en 2020");
        }
        public static void H8_ticks_sinds_2000(){
            DateTime begin2000 = new DateTime(2000,1,1);
            DateTime nuDatum = DateTime.Now;

            long verschilInTicks = nuDatum.Ticks - begin2000.Ticks;
            Console.WriteLine(" Sinds 1 januari 2000 zijn er {0:N0} ticks voorbijgegaan. ", verschilInTicks);
        }
        public static void H8_simpele_timing(){
            DateTime beginTijd = DateTime.Now;

            int[] array= new int[1000000];
            
            for (int i = 0; i < 1000000;i++){
                array[i] = i;
            }                
            DateTime eindTijd = DateTime.Now;
            Console.WriteLine(beginTijd);
            Console.WriteLine(eindTijd);
            TimeSpan verschil  = (eindTijd - beginTijd);
            Console.WriteLine("Het duurt {0:N0} milliseconden om een array van een miljoen elementen aan temaken en op te vullen met opeenvolgende waarden", verschil.Milliseconds);
        }
    }
}
