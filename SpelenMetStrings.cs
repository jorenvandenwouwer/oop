﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class SpelenMetStrings
    {
        public static void VerbatimCharacter()
        {
            string text = "mijn naam is Jef Inghelbrecht \n\t Mijn functie is Docent \n\t AP Hogeschool";
            Console.WriteLine(text);
            string textVerbatim = @"mijn naam is Jef Inghelbrecht
    Mijn functie is Docent
    AP Hogeschool";
            Console.WriteLine(textVerbatim);
        }
        public static void StringsSplitsen()
        {
            string names = "Johan,Farah,Sanne";
            string[] namesSplitted = names.Split(',');
            for(int i = 0; i<namesSplitted.Length; i++)
            {
                Console.WriteLine(namesSplitted[i]);
            }
        }
        public static void StringsSamenvoegen()
        {
            string[] names = { "johan", "Farah", "Sanne" };
            string text = String.Join(";", names);
            Console.WriteLine(text);
        }
        public static void AdereNuttigeMethoden()
        {
            string text = "Dit schrijfblokje is gemaakt van ecologisch papier";
            Console.WriteLine($"lengte: {text.Length}");
            Console.WriteLine($"begin van gemaakt : {text.IndexOf("gemaakt")}");
            Console.WriteLine(text.ToUpper());
            Console.WriteLine(text.Replace("gemaakt", "geproduceerd");
        }
    }
}
