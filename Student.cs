﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
	enum ClassGroups { EA1, EA2, EB1};
	class Student
	{
		public string Name { get; set; }
		private byte age;

		public byte Age
		{
			get { return age; }
			set
			{
				if (value <= 120)
				{
					age = value;
				}
				else
				{
					age = 0;
				}
			}
		}

		public ClassGroups ClassGroup { get; set; }

		private byte markCommunication;

		public byte MarkCommunication
		{
			get { return markCommunication; }
			set
			{
				if (value <= 20)
				{
					markCommunication = value;
				}
			}
		}

		private byte markProgrammingPrinciples;

		public byte MarkProgrammingPrinciples
		{
			get { return markProgrammingPrinciples; }
			set {
				if (value <= 20)
				{
					markProgrammingPrinciples = value;
				}
			}
		}

		private byte markWebTech;

		public byte MarkWebTech
		{
			get { return markWebTech; }
			set
			{
				if (value <= 20)
				{
					markWebTech = value;
				}
			}
		}

		public float OverAllMark { get
			{
				return (MarkCommunication + MarkProgrammingPrinciples
					+ MarkWebTech) / 3;
			}
		}

		public string ShowOverview()
		{
			// We gebruiken het @ symbool aan het begin van de string
			// om een verbatim string literal te maken zodat
			// we de string over meer dan 1 regel kunnen schrijven
			// dit is leesbaarder
			// zie: https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/verbatim
			string view = @$"
{Name}, {(Age==0 ? "Ongeldige leeftijd" : Age.ToString() + " jaar")}
Klasgroep: {ClassGroup}
Cijferrapport:
*************
Communicatie:           {MarkCommunication}
Programming Principles: {MarkProgrammingPrinciples}
Web Technology:         {MarkWebTech}
Gemiddelde:             {OverAllMark}";
			return view;
		}
	}
}
