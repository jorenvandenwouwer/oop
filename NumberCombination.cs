﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class NumberCombination
    {
        int getal1 { get; set; }
        int getal2 { get; set; }
        double Sum()
        {
            return getal1 + getal2;
        }
        double Difference()
        {
            return getal1 - getal2;
        }
        double Product()
        {
            return getal1 * getal2;
        }
        double Quotient()
        {
            if (getal1 == 0 || getal2 == 0)
            {
                Console.WriteLine("error");
            }
            double uitkomst = getal1 / getal2;
            return uitkomst;
        }
        public static void NumberMain()
        {
            NumberCombination pair1 = new NumberCombination();
            pair1.getal1 = 12;
            pair1.getal2= 34;
            Console.WriteLine("Paar:" + pair1.getal1 + ", " + pair1.getal2);
            Console.WriteLine("Sum = " + pair1.Sum());
            Console.WriteLine("Verschil = " + pair1.Difference());
            Console.WriteLine("Product = " + pair1.Product());
            Console.WriteLine("Quotient = " + pair1.Quotient());
        }
    }
}
