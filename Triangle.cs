﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Triangle
    {   
        private double _base = 1;
        private double _height =1;
        double Surface {
            get {
                return _base * _height / 2;
            } 
        }
        double Base
        {
            get
            {
                return _base;
            }
            set
            {
                if( value <= 0)
                {
                    Console.WriteLine("Het is verboden een basis van " + value + " in te stellen");
                } else
                {
                    _base = value;
                }
            }
        }
        double Height {
            get { return _height
                    ; }
            set {
                if( value <= 0)
                {
                    Console.WriteLine("Het is verboden een hoogte van " + value + " in te stellen"); 
                }else
                {
                    _height = value;
                }
            }
        }

        public Triangle(double @base, double height)
        {
            Base = @base;
            Height = height;
            Console.WriteLine("Een rechthoek met een breedte van " + _base + "m en een hoogte van " + _height + "m heeft een oppervlakte van " + Surface + "m².");
        }
    }
}
