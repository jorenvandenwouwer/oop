﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class Rectangle
    {
        private double _width = 1;
        private double _height = 1;
        double Surface
        {
            get
            {
                return _width * _height;
            }
        }

        double Width
        {
            get { return _width; }
            set
            {
                if (value <= 0)
                {
                    Console.WriteLine("Het is verboden een breedte van " + value + "in te stellen");
                }
                else
                {
                    _width = value;
                }
            }
        }
        double Height
        {
            get { return _height; }
            set
            {
                if (value <= 0)
                {
                    Console.WriteLine("Het is verboden een hoogte van " + value + " in te stellen");
                }
                else
                {
                    _height = value;
                }
            }
        }

        public Rectangle(double width, double height)
        {
            Width = width;
            Height = height;
            Console.WriteLine("Een rechthoek met een breedte van "+  _width + "m en een hoogte van " + _height +"m heeft een oppervlakte van "+ Surface +"m².");
        }
    }
}
