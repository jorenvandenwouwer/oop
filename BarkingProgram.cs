﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class BarkingProgram
    {
        // nu maken we onze randomgenerator *buiten* Main
        public static Random rng = new Random();
        public static void BarkingMain()
        {
            BarkingDog dog1 = new BarkingDog();
            BarkingDog dog2 = new BarkingDog();
            dog1.Name = "Swieber";
            dog2.Name = "Misty";
            dog1.Breed = BarkingDog.BreedGenerator();
            dog2.Breed = BarkingDog.BreedGenerator();
            
            while (true)
            {
                Console.WriteLine(dog1.Bark());
                Console.WriteLine(dog2.Bark());
            }
        }
    }
}
