using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{

    class Pokemon {
        public enum Fightoutcome { WIN,LOSS,UNDECIDED};
        private int maxHp;
        static int fightCountGrass = 0;
        static int fightCountFire = 0;
        static int fightCountWater = 0;
        static int fightCountElectric = 0;

        public int MaxHP {
            get {
                return maxHp;
            }
            set {
                if (value < 20) {
                    maxHp = 20;
                } else if (value > 1000) {
                    maxHp = 1000;
                } else {
                    maxHp = value;
                }
            }
        }
        private int hP;
        public int HP {
            get {
                return hP;
            }
            set {
                if (value < 0) {
                    hP = 0;
                } else if (value > maxHp) {
                    hP = maxHp;
                } else {
                    hP = value;
                }
            }
        }
        public PokeTypes PokeType { get; set; }
        public PokeSpecies PokeSpecie { get; set; }
        public void Attack()
        {
            if(PokeType == PokeTypes.Grass)
            {
                fightCountGrass++;
                Console.ForegroundColor = ConsoleColor.Green;
            } else if (PokeType == PokeTypes.Fire)
            {
                fightCountFire++;
                Console.ForegroundColor = ConsoleColor.Red;
            }
            else if (PokeType == PokeTypes.Electric)
            {
                fightCountElectric++;
                Console.ForegroundColor = ConsoleColor.Yellow;
            }
            else if (PokeType == PokeTypes.Water)
            {
                fightCountWater++;
                Console.ForegroundColor = ConsoleColor.Blue;
            }
            Console.WriteLine(PokeSpecie.ToString());
            Console.ForegroundColor = ConsoleColor.Gray;
            
        }

        public Pokemon()
        {

        }
        public Pokemon(int maxHp, int hP, PokeSpecies pokeSpecie, PokeTypes pokeType)
        {
            this.maxHp = maxHp;
            this.hP = hP;
            PokeType = pokeType;
            PokeSpecie = pokeSpecie;
        }

        public Pokemon(int maxHp, PokeSpecies pokeSpecie, PokeTypes pokeType) : this(maxHp, maxHp / 2, pokeSpecie, pokeType) { }  
        public static void ConstructPokemonChained()
        {
            Pokemon squirtle = new Pokemon(20, PokeSpecies.Squirtle, PokeTypes.Water);
            Pokemon bulbasaur = new Pokemon(20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Pokemon charmander = new Pokemon(20, PokeSpecies.Charmander, PokeTypes.Fire);
            Pokemon pikachu = new Pokemon(20, PokeSpecies.Pikachu, PokeTypes.Electric);
            Console.WriteLine($"de nieuwe bulbasaur geeft maximum {bulbasaur.MaxHP} HP en heeft momenteel {bulbasaur.HP}");
            Console.WriteLine($"de nieuwe charmander geeft maximum {charmander.MaxHP} HP en heeft momenteel {charmander.HP}");
            Console.WriteLine($"de nieuwe pikachu geeft maximum {pikachu.MaxHP} HP en heeft momenteel {pikachu.HP}");
            Console.WriteLine($"de nieuwe squirtle geeft maximum {squirtle.MaxHP} HP en heeft momenteel {squirtle.HP}");
        }
        public static void MakePokemon()
        {
           
            Pokemon bulbasaur = new Pokemon(20, 20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Pokemon charmander = new Pokemon(20, 20, PokeSpecies.Charmander, PokeTypes.Fire);
            Pokemon squirtle = new Pokemon(20, 20, PokeSpecies.Squirtle, PokeTypes.Water);
            Pokemon pikachu = new Pokemon(20, 20, PokeSpecies.Pikachu, PokeTypes.Electric);

            
            bulbasaur.Attack();
            charmander.Attack();
            squirtle.Attack();
            pikachu.Attack();
        }
        public static Pokemon FirstConsciousPokemon( Pokemon[] array)
        {
            foreach(Pokemon pokemon in array)
            {
                if(pokemon.HP > 0)
                {
                    return pokemon;
                }
            }
            return null;

        }
        public static void TestConsciousPokemon()
        {
            
            Pokemon bulbasaur = new Pokemon(20, 0, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Pokemon charmander = new Pokemon(20, 0, PokeSpecies.Charmander, PokeTypes.Fire);
            Pokemon squirtle = new Pokemon(20, 2, PokeSpecies.Squirtle, PokeTypes.Water);
            Pokemon pikachu = new Pokemon(20, 20, PokeSpecies.Pikachu, PokeTypes.Electric);
            Pokemon[] array = new Pokemon[] { bulbasaur, charmander, squirtle, pikachu };
            Pokemon conscious = FirstConsciousPokemon(array);
            conscious.Attack();
        }
        public static void TestConsciousPokemonImproved()
        {

            Pokemon bulbasaur = new Pokemon(20, 0, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Pokemon charmander = new Pokemon(20, 0, PokeSpecies.Charmander, PokeTypes.Fire);
            Pokemon squirtle = new Pokemon(20, 0, PokeSpecies.Squirtle, PokeTypes.Water);
            Pokemon pikachu = new Pokemon(20, 0, PokeSpecies.Pikachu, PokeTypes.Electric);
            Pokemon[] array = new Pokemon[] { bulbasaur, charmander, squirtle, pikachu };
            Pokemon conscious = FirstConsciousPokemon(array);
            if (conscious is null)
            {
                Console.WriteLine("Al je Pok�mon zijn KO! Haast je naar het Pok�mon center.");
            }
            else
            {
                conscious.Attack();
            }
        }
        public static void RestoreHP(Pokemon pokemon)
        {
            pokemon.HP = pokemon.MaxHP;
        }
        public static void DemoRestoreHP()
        {
            Pokemon bulbasaur = new Pokemon(20, 0, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Pokemon charmander = new Pokemon(20, 0, PokeSpecies.Charmander, PokeTypes.Fire);
            Pokemon squirtle = new Pokemon(20, 2, PokeSpecies.Squirtle, PokeTypes.Water);
            Pokemon pikachu = new Pokemon(20, 20, PokeSpecies.Pikachu, PokeTypes.Electric);
            Pokemon[] array = new Pokemon[] { bulbasaur, charmander, squirtle, pikachu };
            for (int i = 0; i < array.Length; i++)
            {
                Pokemon.RestoreHP(array[i]);
            }
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i].HP);
            }
        }
        public static Fightoutcome FightOutcome(Pokemon poke1,Pokemon poke2, Random random)
        {
            if (poke1 is null || poke2 is null)
            {
                Console.WriteLine(" zorg ervoor dat beide pokemons bestaan !");
                return Fightoutcome.UNDECIDED;
            }
            else if (poke1.HP == 0 && poke2.HP == 0)
            {
                return Fightoutcome.UNDECIDED;
            }
            else if (poke1.HP == 0)
            {
                return Fightoutcome.LOSS;
            }
            else if (poke2.HP == 0)
            {
                return Fightoutcome.WIN;
            }
            else
            {
                bool pokemonDied = false;
                if (random.Next() % 2 == 0)
                {
                    while (pokemonDied != true)
                    {
                        poke1.Attack();
                        poke2.HP -= random.Next(0, 20);
                        if (poke2.HP == 0)
                        {
                            pokemonDied = true;
                        }
                        else
                        {
                            poke2.Attack();
                            poke1.HP -= 2;
                            if (poke1.HP == 0)
                            {
                                pokemonDied = true;
                            }
                        }
                    }
                }
                else
                {
                    while (pokemonDied != true)
                    {
                        poke2.Attack();
                        poke1.HP -= random.Next(0, 20);
                        if (poke1.HP == 0)
                        {
                            pokemonDied = true;
                        }
                        else
                        {
                            poke1.Attack();
                            poke2.HP -= 2;
                            if (poke2.HP == 0)
                            {
                                pokemonDied = true;
                            }
                        }
                    }
                }
                if (poke1.HP == 0)
                {
                    return Fightoutcome.LOSS;
                }
                else
                {
                    return Fightoutcome.WIN;
                }
            }
        }
        public static void DemoFightOutcome()
        {
            Random rand = new Random();
            // twee gezonde
            Pokemon bulbasaur = new Pokemon(20, 4, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Pokemon charmander = new Pokemon(20, 4, PokeSpecies.Charmander, PokeTypes.Fire);
            FightOutcome(bulbasaur, charmander, rand);
            // een bewusteloos
            Pokemon squirtle = new Pokemon(20, 0, PokeSpecies.Squirtle, PokeTypes.Water);
            Pokemon pikachu = new Pokemon(20, 4, PokeSpecies.Pikachu, PokeTypes.Electric);
            FightOutcome(squirtle, pikachu, rand);
            // twee bewusteloos
            Pokemon poke1 = new Pokemon(20, 0, PokeSpecies.Squirtle, PokeTypes.Water);
            Pokemon poke2 = new Pokemon(20, 0, PokeSpecies.Pikachu, PokeTypes.Electric);
            FightOutcome(poke1, poke2, rand);
            //een null
            Pokemon poke3 = null;
            Pokemon poke4 = new Pokemon(20, 4, PokeSpecies.Pikachu, PokeTypes.Electric);
            FightOutcome(poke3, poke4, rand);
            // twee null
            Pokemon poke5 = null;
            Pokemon poke6 = null;
            FightOutcome(poke5, poke6, rand);
            // een bewusteloos en een null
            Pokemon poke7 = new Pokemon(20, 0, PokeSpecies.Squirtle, PokeTypes.Water);
            Pokemon poke8 = null;
            FightOutcome(poke7, poke8, rand);
        }
        public static void DemonstrateCounter()
        {
            
            Pokemon bulbasaur = new Pokemon(20, 20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Pokemon charmander = new Pokemon(20, 20, PokeSpecies.Charmander, PokeTypes.Fire);
            Pokemon squirtle = new Pokemon(20, 20, PokeSpecies.Squirtle, PokeTypes.Water);
            Pokemon pikachu = new Pokemon(20, 20, PokeSpecies.Pikachu, PokeTypes.Electric);
            Pokemon bulbasaur2 = new Pokemon(20, 20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Random rand = new Random();
            for (int i = 0; i < rand.Next(5, 10); i++)
            {
                bulbasaur.Attack();
            }
            for (int i = 0; i < rand.Next(5, 10); i++)
            {
                charmander.Attack();
            }
            for (int i = 0; i < rand.Next(5, 10); i++)
            {
                squirtle.Attack();
            }
            for (int i = 0; i < rand.Next(5, 10); i++)
            {
                pikachu.Attack();
            }
            for (int i = 0; i < rand.Next(5, 10); i++)
            {
                bulbasaur2.Attack();
            }
            Console.WriteLine($"Aantal aanvallen van Pok�mon met type `Grass`:{fightCountGrass}");
            Console.WriteLine($"Aantal aanvallen van Pok�mon met type `Fire`:{fightCountFire}");
            Console.WriteLine($"Aantal aanvallen van Pok�mon met type `Water`:{fightCountWater}");
            Console.WriteLine($"Aantal aanvallen van Pok�mon met type `Electric`:{fightCountElectric}");
        }
    }
}

