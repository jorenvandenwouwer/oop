﻿using System;
using Wiskunde.Meetkunde;
using OOP;

namespace LerenWerkenMetOOP
{
    class Program
    {
        static void Main(string[] args)
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("Hoofdmenu");
                Console.WriteLine("1: Beginnen met OO");
                Console.WriteLine("2: DateTime: leren werken met objecten");
                Console.WriteLine("3: De Studentklasse testen");
                Console.WriteLine("4: Geheugenmanagment bij klassen");
                Console.WriteLine("5: Geavanceerde klassen");
                Console.WriteLine("6: Spelen met Strings");
                Console.WriteLine("Q. Het is genoeg voor vandaag!");
                char response = Console.ReadKey().KeyChar;
                Console.WriteLine();
                switch (response)
                {
                    case '1':
                        SubMenuBeginnenMetOO();
                        break;
                    case '2':
                        SubMenuDateTime();
                        break;
                    case '3':
                        TestStudent();
                        break;
                    case '4':
                        SubMenuGeheugenManagment();
                        break;
                    case '5':
                        SubMenuGeavanceerdeKlassen();
                        break;
                    case '6':
                        SubMenuSpelenMetStrings();
                        break;
                    case 'Q':
                        Console.WriteLine("Ga je ermee stoppen? J/N");
                        if (Console.ReadKey().KeyChar == 'J')
                        {
                            done = true;
                        }
                        break;
                }
                Console.Clear();
            }
            Console.ReadKey();
        }
        private static void SubMenuSpelenMetStrings()
        {
            bool done  = false;
            while (!done)
            {
                Console.WriteLine("1. Verbatim character");
                Console.WriteLine("2. Split");
                Console.WriteLine("3. Join");
                Console.WriteLine("4. AndereNuttigeMethoden");
                Console.WriteLine("Q. Terug naar hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                Console.WriteLine("");
                switch (response)
                {
                    case '1':
                        SpelenMetStrings.VerbatimCharacter();
                        break;
                    case '2':
                        SpelenMetStrings.StringsSplitsen();
                        break;
                    case '3':
                        SpelenMetStrings.StringsSamenvoegen();
                        break;
                    case '4':
                        SpelenMetStrings.AdereNuttigeMethoden();
                        break;
                    case 'Q':
                        done = true;
                        break;
                }
            }
        }
        private static void SubMenuGeavanceerdeKlassen()
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("1: tombola");
                char response = Console.ReadKey().KeyChar;
                Console.WriteLine();
                switch (response)
                {
                    case '1':
                        Ticket.Raffle();
                        break;
                    case 'Q':
                        Console.WriteLine("Ga je ermee stoppen? J/N");
                        if (Console.ReadKey().KeyChar == 'J')
                        {
                            done = true;
                        }
                        break;
                }
            }

        }

        static void TestStudent()
        {
            OOP.Student student1 = new Student();
            student1.ClassGroup = ClassGroups.EA2;
            student1.Age = 21;
            student1.Name = "Bob Dylan";
            student1.MarkCommunication = 18;
            student1.MarkProgrammingPrinciples = 19;
            student1.MarkWebTech = 20;
            string text = student1.ShowOverview();
            Console.WriteLine(text);

            Student student2 = new Student();
            student2.ClassGroup = ClassGroups.EB1;
            student2.Age = 130;
            student2.Name = "Sareh El Farisi";
            student2.MarkCommunication = 19;
            student2.MarkProgrammingPrinciples = 19;
            student2.MarkWebTech = 20;
            text = student2.ShowOverview();
            Console.WriteLine(text);
 
            Console.ReadKey();
        }
        static void SubMenuBeginnenMetOO()
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("Submenu Beginnen met OO");
                Console.WriteLine("1. Wat is static?");
                Console.WriteLine("2. Public vs Private");
                Console.WriteLine("3. Teken een lijn");
                Console.WriteLine("4. Teken een lijn met een *");
                Console.WriteLine("5. Teken een lijn met 20 *en");
                Console.WriteLine("6. Teken een lijn met de standaard static teken waarde");
                Console.WriteLine("7. Teken een lijn met de static teken waarde = +");
                Console.WriteLine("8. Teken een rechthoek 10 tekens lang en 3 lijnen hoog");
                Console.WriteLine("9. Teken een volle rechthoek en geef hoogte en breedte op");
                Console.WriteLine("a. Teken een lege rechthoek en geef hoogte en breedte op");
                Console.WriteLine("Q. Terug naar het hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                switch (response)
                {
                    case '1':
                        WatIsStatic();
                        break;
                    case '2':
                        PrivateVsPublic();
                        break;
                    case '3':
                        /* ik kan de methode Lijn oproepen
                         * omdat die static is en ik hoef
                         * dus niet eerst een object of instantie
                         * te maken */
                        Console.WriteLine(Vormen.Lijn());
                        break;
                    case '4':
                        Console.WriteLine(Vormen.Lijn('*'));
                        break;
                    case '5':
                        Console.WriteLine(Vormen.Lijn('*', 20));
                        break;
                    case '6':
                        Console.WriteLine(Vormen.LijnDieHetVeldMetDeNaamTekenGebruikt());
                        break;
                    case '7':
                        Vormen.teken = '+';
                        Console.WriteLine(Vormen.LijnDieHetVeldMetDeNaamTekenGebruikt());
                        break;
                    case '8':
                        Console.WriteLine(Vormen.VolleRechthoek());
                        break;
                    case '9':
                        Console.WriteLine(Vormen.VolleRechthoek(20, 20));
                        break;
                    case 'a':
                        Console.WriteLine(Vormen.LegeRechthoek(20, 20));
                        break;

                    case 'Q':
                        done = true;
                        break;
                }
            }
        }

        static void SubMenuGeheugenManagment(){
            bool done =false;
            while(!done){
                Console.WriteLine("submenu Geheugenmanagment bij klassen");
                Console.WriteLine("1: pokeAttack");
                Console.WriteLine("2: consciousPokemon");
                Console.WriteLine("3: consciousPokemonImproved");
                Console.WriteLine("4: pokevalueref");
                Console.WriteLine("5: fight");
                Console.WriteLine("6: pokemon constructor chaining");
                Console.WriteLine("7: pokebattlecount");
                Console.WriteLine("q : quit");
                char response = Console.ReadKey().KeyChar;
                switch(response){
                    case '1' :
                        OOP.Pokemon.MakePokemon();
                        break;
                    case '2':
                        OOP.Pokemon.TestConsciousPokemon();
                        break;
                    case '3':
                        OOP.Pokemon.TestConsciousPokemonImproved();
                        break;
                    case '4':
                        OOP.Pokemon.DemoRestoreHP();
                        break;
                    case '5':
                        OOP.Pokemon.DemoFightOutcome();
                        break;
                    case '6':
                        OOP.Pokemon.ConstructPokemonChained();
                        break;
                    case '7':
                        OOP.Pokemon.DemonstrateCounter();
                        break;
                    case 'q':
                        done = true;
                        break;
                }
            }
        }
        static void SubMenuDateTime()
        {
            bool done = false;
            while (!done)
            {
                Console.WriteLine("Submenu DateTime: leren werken met objecten");
                Console.WriteLine("1: H8 Dag van de week");
                Console.WriteLine("2: H8 Schrikkeljaar teller");
                Console.WriteLine("3: H8 ticks sinds 2000");
                Console.WriteLine("4: H8 simpele timing");
                Console.WriteLine("5: H8 ResultV1");
                Console.WriteLine("6: H8 ResultV2");
                Console.WriteLine("7: H8 NumberCombination");
                Console.WriteLine("8: H8 Figuren");
                Console.WriteLine("9: H8 Honden");
                Console.WriteLine("Q. Terug naar hoofdmenu");
                char response = Console.ReadKey().KeyChar;
                switch (response)
                {
                    case '1':
                        OOP.LerenWerkenMetDateTime.DayOfWeek();
                        break;
                    case '2':
                        OOP.LerenWerkenMetDateTime.LeapYear();
                        break;
                    case '3':
                        OOP.LerenWerkenMetDateTime.H8_ticks_sinds_2000();
                        break;
                    case '4':
                        OOP.LerenWerkenMetDateTime.H8_simpele_timing();
                        break;
                    case '5':
                        OOP.ResultV1.ResultMain();
                        break;
                    case '6':
                        OOP.ResultV2.ResultMain();
                        break;
                    case '7':
                        OOP.NumberCombination.NumberMain();
                        break;
                    case '8':
                        OOP.FigureProgram.FigurenMain();
                        break;
                    case '9':
                        OOP.BarkingProgram.BarkingMain();
                        break;
                    case 'Q':
                        done = true;
                        break;
                }
            }
        }
        static void WatIsStatic()
        {
            Console.WriteLine("Leren werken met DGP 'ding gericht programmeren!");
            // instantie of exemplaar van de klasse maken
            // Wiskunde.Meetkunde.Vormen vormen = new Wiskunde.Meetkunde.Vormen();
            // de methode van de instantie of het object gebruiken
            // Console.WriteLine(vormen.Lijn());
            // als de methode lijn static is, behoort die tot
            // de klasse en niet tot het object
            Console.WriteLine(Wiskunde.Meetkunde.Vormen.Lijn());
        }

        static void PrivateVsPublic()
        {
            // het veld kleur van de klasse Vormen is hier
            // zichtbaar omdat het public is
            Wiskunde.Meetkunde.Vormen.kleur = ConsoleColor.Red;
            Wiskunde.Meetkunde.Vormen vormen = new Wiskunde.Meetkunde.Vormen();
            // het object, instantie of exemplaar geeft geen toegang 
            // tot static methode
            // vormen.LijnInKleuren();
            Wiskunde.Meetkunde.Vormen.Achtergrond = ConsoleColor.Cyan;
            Wiskunde.Meetkunde.Vormen.LijnInKleur();
            // welke kleur wordt door Vormen gebruikt?
            if (Wiskunde.Meetkunde.Vormen.Achtergrond == ConsoleColor.Red)
            {
                Console.WriteLine("De achtergrondkleur is rood.");
            }
            else
            {
                Console.WriteLine("De achtergrondkleur is niet rood.");

            }
        }
        public static void DemonstrateCounter()
        {
            int fightCountGrass = 0;
            int fightCountFire = 0;
            int fightCountWater = 0;
            int fightCountElectric = 0;
            Pokemon bulbasaur = new Pokemon(20, 20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Pokemon charmander = new Pokemon(20, 20, PokeSpecies.Charmander, PokeTypes.Fire);
            Pokemon squirtle = new Pokemon(20, 20, PokeSpecies.Squirtle, PokeTypes.Water);
            Pokemon pikachu = new Pokemon(20, 20, PokeSpecies.Pikachu, PokeTypes.Electric);
            Pokemon bulbasaur2 = new Pokemon(20, 20, PokeSpecies.Bulbasaur, PokeTypes.Grass);
            Random rand = new Random();
            for (int i = 0; i < rand.Next(5, 10); i++)
            {
                bulbasaur.Attack();
            }
            for (int i = 0; i < rand.Next(5, 10); i++)
            {
                charmander.Attack();
            }
            for (int i = 0; i < rand.Next(5, 10); i++)
            {
                squirtle.Attack();
            }
            for (int i = 0; i < rand.Next(5, 10); i++)
            {
                pikachu.Attack();
            }
            for (int i = 0; i < rand.Next(5, 10); i++)
            {
                bulbasaur2.Attack();
            }
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Grass`:{fightCountGrass}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Fire`:{fightCountFire}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Water`:{fightCountWater}");
            Console.WriteLine($"Aantal aanvallen van Pokémon met type `Electric`:{fightCountElectric}");
        }
    }
}
