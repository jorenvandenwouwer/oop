﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class BarkingDog
    {
        public string Name;
        public string Breed;

        public string Bark()
        {
            if (Breed == "German Shepherd")
            {
                return "RUFF!";
            }
            else if (Breed == "Wolfspitz")
            {
                return "AwawaWAF!";
            }
            else if (Breed == "Chihuahua")
            {
                return "ARF ARF ARF!";
            }
            // dit zou nooit mogen gebeuren
            // maar als de programmeur van Main iets fout doet, kan het wel
            else
            {
                return "Euhhh... Miauw?";
            }
        }
        public static string BreedGenerator()
        {

            Random rand = new Random();
            int breedNumber = rand.Next(2);
            if(breedNumber == 0)
            {
                return "German Shepherd";
            } else if ( breedNumber == 1)
            {
                return "Wolfspitz";
            } else
            {
                return "Chihuahua";
            }
        }
    }
}
