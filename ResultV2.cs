﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OOP
{
    class ResultV2
    {   
        public enum Honors { Nietgeslaagd,voldoende,onderscheiding,GroteOnderscheiding,GrootsteOnderscheiding};
        public int percent { get; set; }
        public Honors ComputeHonors() {
			if (percent < 50)
			{
				return Honors.Nietgeslaagd;
			}
			else if (percent < 68)
			{
				return Honors.voldoende;
			}
			else if (percent < 75)
			{
				return Honors.onderscheiding;
			}
			else if (percent < 85)
			{
				return Honors.GroteOnderscheiding;
			}
			else
			{
				return Honors.GrootsteOnderscheiding;
			}
		}
		public static void ResultMain()
		{
			ResultV2 result = new ResultV2();
			result.percent = 20;

			ResultV2 result1 = new ResultV2();
			result1.percent = 51;

			ResultV2 result2 = new ResultV2();
			result2.percent = 69;

			ResultV2 result3 = new ResultV2();
			result2.percent = 76;

			ResultV2 result4 = new ResultV2();
			result4.percent = 95;
			result.ComputeHonors();
			result1.ComputeHonors();
			result2.ComputeHonors();
			result3.ComputeHonors();
			result4.ComputeHonors();
		}
    }
}
